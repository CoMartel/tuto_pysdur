"""This is a simple module to evaluate PGCD of two numbers"""


def order(a, b):
    """
    Order two integer a > b, a!=b
    :param a: int
    :param b: int
    :return: tuple (a,b), a > b
    """
    if b > a:  # order a and b
        a, b = b, a
    return a, b


def pgcd(a, b):
    """
    Return the largest common denominator of a and b
    :param a: integer
    :param b: integer
    :return: int
    """
    a, b = order(a, b)

    while a - b != 0:
        c = a - b
        a, b = min(a, b), c
        a, b = order(a, b)

    return b


if __name__ == "__main__":
    for i, j in zip([10, 20, 451, 13, 81], [2, 18, 21, 63, 54]):
        print(f"PGCD of {i}, {j} = {pgcd(i,j)}")
